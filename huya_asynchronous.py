#-*-coding:utf-8-*-
#!/usr/bin/python

from lib import config, req_sy, db_sy
from lib import email_sy as email
from lib.logger import logger
from bs4 import BeautifulSoup
import time
import re
import gevent.monkey
gevent.monkey.patch_socket()
import gevent
from gevent.pool import Pool

onlive_src = 'huya'
gpool = Pool(config.GPOOLSIZE)

# 处理来虎牙的的直播视频 直播地址：http://www.huya.com/huli666
def _pro_onlivedata_from_huya(zbid):
	url = config.HUYA_VIEW % zbid
	#logger.info('%s | %s ' % (u'huya', url))
	html = req_sy.get_html(url)
	if html.strip() != '':
		try:
			soup = BeautifulSoup(html, 'html.parser', from_encoding = 'utf-8')
			# 查找当前直播地址
			v_obj = soup.find(id='flash-link')
			if v_obj:
				return zbid, v_obj['value']
		except Exception as e:
			reason = "%s : %s" % (u'获取huya直播地址', e)
			logger.error(reason)
			raise 

	return zbid, ''

def _get_page_views_asynchronous(items):
	threads = []
	for zb in items:
		zbid = zb['privateHost']
		threads.append(gpool.spawn(_pro_onlivedata_from_huya, zbid))
	gpool.join()

	res = {}
	is_error = False
	reason = None
	# 遍历threads, 判断是否成功
	for thread in threads:
		if thread.successful():
			res[thread.value[0]] = thread.value[1]
		else:
			is_error = True
			reason = thread.exception
	# 若执行过程出错，发送邮件
	if is_error:
		email.send_email(u'huya直播爬取error', str(reason))
	return res

def get_data_action(is_get_livedata = True):
	game_type = config.HUYA_GAME
	page = 1
	# huya视频列表页api:http://www.huya.com/index.php?m=Live&do=ajaxAllLiveByPage&page=4&pageNum=1
	# 在线人数作为是否继续爬取的判断条件
	top_views = 100
	res_data = []
	while page < 200 and top_views > 50:
		if not is_get_livedata:
			time.sleep(0.1)
		top_views = 49
		huya_api = config.HUYA_API % page
		response = req_sy.get_html(huya_api, is_json=True)
		if response and len(response['data']) != 0:
			res_list = response['data']['list']
			# 是否单个页面爬取在线直播地址
			if is_get_livedata:
				livedatas = _get_page_views_asynchronous(res_list)
			for i, zb in enumerate(res_list):
				kw = {}
				gameHostName = zb['gameHostName']
				if gameHostName not in game_type:
					continue
				views = int(zb['totalCount'])
				if top_views < views:
					top_views = views
				#logger.info(str(page) + ', views:'+ str(views) + ',top_views:'+ str(top_views) + ',' + gameHostName)
				kw['zbid'] = zb['privateHost']
				if is_get_livedata:
					kw['livedata'] = livedatas[zb['privateHost']]
				kw['zbname'] = zb['nick']
				kw['source'] = onlive_src
				kw['views'] = views
				kw['title'] = zb['introduction']
				kw['category'] = game_type[gameHostName]
				kw['inputtime'] = int(time.time())
				kw['zb_thumb'] = zb['avatar180']
				kw['thumb'] = zb['screenshot']
				res_data.append(kw)
		else:
			logger.error("%s | %s" % ("get huya API Failed", huya_api))
			email.send_email(u'huya直播API获取失败', "%s | %s" % ("get huya API Failed", huya_api))
			top_views = 100
		page = page + 1

	return res_data

if __name__ == "__main__":
	now =  time.time()
	dbconn = db_sy.getConnection()
	update_data = get_data_action(is_get_livedata = True)
	db_sy.db_insert(dbconn, update_data)
	dbconn.close()
	print "time cost : " + str(int((time.time() - now))) + " seconds"