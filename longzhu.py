#-*-coding:utf-8-*-
#!/usr/bin/python

from lib import config, req_sy, json_sy
from bs4 import BeautifulSoup
import codecs
import onliveDown
import time
import re

onlive_src = 'longzhu'

# 处理来源于龙珠的直播视频，直播地址： http://star.longzhu.com/134903
def pro_onlivedata_from_longzhu(vid, room_id, f_url_ex, url_pre = 'http://star.longzhu.com/'):
	# without vid : http://r.plures.net/proton/flash/streaming-ifp2rgic.swf?hasNextBtn=0&amp;hasMovieBtn=0&amp;autoPlay=1&amp;roomId=218762
	# vid : 'http://imgcache.qq.com/minivideo_v1/vd/res/TencentPlayerLive.swf?max_age=86400&amp;vid=' + player_vid + '&amp;vurl=http://zb.v.qq.com:1863/?progid=' + player_vid + '&amp;showcfg=1&amp;share=1&amp;full=1&amp;autoplay=1&amp;scale=full&amp;p=true&amp;loadingswf=http://imgcache.qq.com/minivideo_v1/vd/res/skins/longzhu_loading.swf'
	if int(vid) == 0:
		return 'http://r.plures.net/proton/flash/streaming-ifp2rgic.swf?hasNextBtn=0&amp;hasMovieBtn=0&amp;autoPlay=1&amp;roomId=' + str(room_id)
	else:
		return 'http://imgcache.qq.com/minivideo_v1/vd/res/TencentPlayerLive.swf?max_age=86400&amp;vid=' + str(vid) + '&amp;vurl=http://zb.v.qq.com:1863/?progid=' + str(vid) + '&amp;showcfg=1&amp;share=1&amp;full=1&amp;autoplay=1&amp;scale=full&amp;p=true&amp;loadingswf=http://imgcache.qq.com/minivideo_v1/vd/res/skins/longzhu_loading.swf&amp;'


def longzhu_insert_action(dbconn):
	game_type = config.LONGZHU_GAME
	file_name = onlive_src + '.log'
	file_err_name = onlive_src + '_err.log'
	file_url_ex = onlive_src + '_pagedown_err.log'
	f = codecs.open(file_name, 'w', 'utf8')
	f_err = codecs.open(file_err_name, 'a+', 'utf8')
	f_url_ex = codecs.open(file_url_ex, 'a+', 'utf8')
	# longzhu视频列表页api:http://api.plu.cn/tga/streams/?game=0&max-results=30&start-index=60&sort-by=top&filter=0
	page_size = 30
	page = 0
	# 在线人数作为是否继续爬取的判断条件
	top_views = 100
	while page < 2 and top_views > 50:
		top_views = 49
		start_index = page * page_size
		cursor = dbconn.cursor()
		longzhu_api = config.LONGZHU_API % (page_size, start_index)
		print longzhu_api
		response = json_sy.request_ajax_url(longzhu_api)
		# 间隔 0.5 秒
		time.sleep(0.2)
		if len(response['data']) != 0:
			res_list = response['data']['items']
			for zb in res_list:
				gameHostName = zb['game'][0]['tag']
				if gameHostName not in game_type:
					continue
				views = int(zb['viewers'])
				if top_views < views:
					top_views = views
				print str(page) + ', views:'+ str(views) + ',top_views:'+ str(top_views) + ',' + gameHostName +'\r\n'
				channel = zb['channel']
				zbid = channel['domain']
				room_id = channel['id']
				vid = channel['vid']
				livedata = pro_onlivedata_from_longzhu(vid, room_id,f_url_ex)
				zbname = channel['name']
				source = onlive_src
				title = channel['status']
				category = game_type[gameHostName]
				inputtime = int(time.time())
				zb_thumb = channel['avatar']
				thumb = zb['preview']
				try:
					cursor.execute("INSERT INTO `sy_video_onlive` (`zbid`, `zbname`, `source`, `title`, `views`, `category`, `isOnlive`, `inputtime`, `zb_thumb`, `thumb`, `livedata`) VALUES (%s, %s, %s, %s, %s, %s, 1, %s, %s, %s, %s)",(zbid, zbname, source, title, views, category, inputtime, zb_thumb, thumb, livedata))
				except Exception, e2:
					print Exception, e2
					print >> f_err, Exception, e2
				dbconn.commit()
		cursor.close()
		page = page + 1
	f.close()
	f_err.close()

if __name__ == "__main__":
	dbconn = onliveDown.getConnection()
	longzhu_insert_action(dbconn)