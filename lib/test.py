#-*-coding:utf-8-*-
#!/usr/bin/python

#
#   Message Bomb v1.0
#From: sb.f4ck.net  By: xfkxfk
#

import json
import urllib2
import sys

def request_ajax_url(url,body,referer=None,cookie=None,**headers):
    req = urllib2.Request(url)

    req.add_header('Content-Type', 'application/json')
    req.add_header('X-Requested-With','XMLHttpRequest')

    if cookie:
        req.add_header('Cookie',cookie)

    if referer:
        req.add_header('Referer',referer)

    if headers:
        for k in headers.keys():
            req.add_header(k,headers[k])

    postBody = json.dumps(body)

    response = urllib2.urlopen(req, postBody)

    if response:
        return response

def run():
    vid = 'XMTM4Mzk1MjQ3Ng=='
    url = 'http://v.youku.com/player/getPlayList/VideoIDS/' + vid
    login_body = {}
    login_referer = ""

    response = request_ajax_url(url,login_body,login_referer)

    res_arr = json.load(response)
    print res_arr['data'][0]['username']





if __name__ == "__main__":
    run()