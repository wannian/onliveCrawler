#-*-coding:utf-8-*-
#!/usr/bin/python

from lib import config, req_sy, db_sy
from lib import email_sy as email
from lib.logger import logger
import codecs
import time
import re
import gevent.monkey
gevent.monkey.patch_socket()
import gevent
from gevent.pool import Pool

onlive_src = 'longzhu'
gpool = Pool(config.GPOOLSIZE)

# 处理来源于龙珠的直播视频，直播地址： http://star.longzhu.com/134903
def _pro_onlivedata_from_longzhu(vid, room_id):
	# without vid : http://r.plures.net/proton/flash/streaming-ifp2rgic.swf?hasNextBtn=0&amp;hasMovieBtn=0&amp;autoPlay=1&amp;roomId=218762
	# vid : 'http://imgcache.qq.com/minivideo_v1/vd/res/TencentPlayerLive.swf?max_age=86400&amp;vid=' + player_vid + '&amp;vurl=http://zb.v.qq.com:1863/?progid=' + player_vid + '&amp;showcfg=1&amp;share=1&amp;full=1&amp;autoplay=1&amp;scale=full&amp;p=true&amp;loadingswf=http://imgcache.qq.com/minivideo_v1/vd/res/skins/longzhu_loading.swf'
	if int(vid) == 0:
		return 'http://r.plures.net/proton/flash/streaming-ifp2rgic.swf?hasNextBtn=0&amp;hasMovieBtn=0&amp;autoPlay=1&amp;roomId=' + str(room_id)
	else:
		#return 'http://imgcache.qq.com/minivideo_v1/vd/res/TencentPlayerLive.swf?max_age=86400&amp;vid=' + str(vid) + '&amp;vurl=http://zb.v.qq.com:1863/?progid=' + str(vid) + '&amp;showcfg=1&amp;share=1&amp;full=1&amp;autoplay=1&amp;scale=full&amp;p=true&amp;loadingswf=http://imgcache.qq.com/minivideo_v1/vd/res/skins/longzhu_loading.swf&amp;'
		return 'http://imgcache.qq.com/minivideo_v1/vd/res/TencentPlayerLive.swf?max_age=86400&amp;vid=' + str(vid) + '&amp;vurl=http://zb.v.qq.com:1863/?progid=' + str(vid) + '&amp;showcfg=1&amp;share=1&amp;full=1&amp;autoplay=1&amp;scale=full&amp;p=true&amp;loadingswf=http://imgcache.qq.com/minivideo_v1/vd/res/skins/longzhu_loading.swf&amp;'

def get_data_action(is_get_livedata = True):
	game_type = config.LONGZHU_GAME
	# longzhu视频列表页api:http://api.plu.cn/tga/streams/?game=0&max-results=30&start-index=60&sort-by=top&filter=0
	page_size = 30
	page = 0
	# 在线人数作为是否继续爬取的判断条件
	top_views = 100
	res_data = []
	while page < 100 and top_views > 50:
		# 加入爬取间隙
		if is_get_livedata:
			time.sleep(1.5)
		top_views = 49
		start_index = page * page_size
		longzhu_api = config.LONGZHU_API % (page_size, start_index)
		response = req_sy.get_html(longzhu_api, is_json=True)
		if response and len(response['data']) != 0:
			# 添加终止条件
			if response['data']['totalItems'] == 0:
				break
			res_list = response['data']['items']
			for zb in res_list:
				kw = {}
				gameHostName = zb['game'][0]['tag']
				if gameHostName not in game_type:
					continue
				views = int(zb['viewers'])
				if top_views < views:
					top_views = views
				#logger.info(str(page) + ', views:'+ str(views) + ',top_views:'+ str(top_views) + ',' + gameHostName)
				channel = zb['channel']
				kw['zbid'] = channel['domain']
				room_id = channel['id']
				vid = channel['vid']
				if is_get_livedata:
					kw['livedata'] = _pro_onlivedata_from_longzhu(vid, room_id)
				kw['zbname'] = channel['name']
				kw['source'] = onlive_src
				kw['views'] = views
				kw['title'] = channel['status']
				kw['category'] = game_type[gameHostName]
				kw['inputtime'] = int(time.time())
				kw['zb_thumb'] = channel['avatar']
				kw['thumb'] = zb['preview']
				res_data.append(kw)
		else:
			logger.error("%s | %s" % ("get "+onlive_src+" API Failed", longzhu_api))
			email.send_email(onlive_src+u'直播API获取失败', "%s | %s" % ("get Panda API Failed", longzhu_api))
			top_views = 100
		page = page + 1
	return res_data



if __name__ == "__main__":
	now =  time.time()
	dbconn = db_sy.getConnection()
	update_data = get_data_action(is_get_livedata = True)
	db_sy.db_insert(dbconn, update_data)
	dbconn.close()
	print "time cost : " + str(int((time.time() - now))) + " seconds"
