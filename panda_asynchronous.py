#-*-coding:utf-8-*-
#!/usr/bin/python

from lib import config, req_sy, db_sy
from lib import email_sy as email
from lib.logger import logger
from bs4 import BeautifulSoup
import time
import re
import gevent.monkey
gevent.monkey.patch_socket()
import gevent
from gevent.pool import Pool

onlive_src = 'panda'
gpool = Pool(config.GPOOLSIZE)

# 处理来源熊猫的的直播视频 直播地址：http://www.panda.tv/666666
def _pro_onlivedata_from_panda(zbid, room_key):
	url = config.PANDA_VIEW % zbid
	#logger.info('%s | %s ' % (u'panda', url))
	res_code, html = req_sy.get_html_2(url)
	# 如果发生跳转，则重新发送请求（针对panda平台上的活动）
	if res_code in [301, 302]:
		url = config.PANDA_VIEW % 'liveframe/' + zbid
		html = req_sy.get_html(url)
		print "redirect: " + url
	if html.strip() != '':
		try:
			soup = BeautifulSoup(html, 'html.parser', from_encoding = 'utf-8')
			# 查找当前直播地址
			text = soup.find(text = re.compile("PANDA_PLAYER_URL"))
			matchObj = re.search(r'URL\":\"(.*)\",\"CHAT_SWF_URL', text)
			if matchObj:
				player_url = matchObj.group(1)
				#player_url = 'http://s4.pdim.gs/static/c03726b8c4124c7a.swf'
				# src = "http://s4.pdim.gs/static/1a788bf39f2f5050.swf?sign=true&amp;videoId=92d768563027d708b0cb81793d93b3bf&amp;
				return zbid, player_url + "?sign=true&amp;videoId=" + room_key + "&amp;isAnchor=false&amp;plflag=2_3"
		except Exception as e:
			reason = "%s : %s, zbid:%s" % (u'获取panda直播地址', e, zbid)
			logger.error(reason)
			email.send_email(u'panda直播爬取error', reason)
	return zbid, ''

def _get_page_views_asynchronous(items):
	threads = []
	for zb in items:
		# 如果zbid except,跳过
		zbid = zb['id']
		room_key = zb['room_key']
		threads.append(gpool.spawn(_pro_onlivedata_from_panda, zbid, room_key))
	gpool.join()

	res = {}
	is_error = False
	reason = None
	# 遍历threads, 判断是否成功
	for thread in threads:
		if thread.successful():
			res[thread.value[0]] = thread.value[1]
		else:
			is_error = True
			reason = thread.exception
	# 若执行过程出错，发送邮件
	# if is_error:
	# 	email.send_email(u'panda直播爬取error', str(reason))
	return res

def get_data_action(is_get_livedata = True):
	game_type = config.PANDA_GAME
	page = 1
	# panda视频列表页api:http://www.panda.tv/live_lists?status=2&order=person_num&pageno=2
	# 在线人数作为是否继续爬取的判断条件
	top_views = 100
	res_data = []
	while page < 200 and top_views > 50:
		if not is_get_livedata:
			time.sleep(0.1)
		top_views = 49
		panda_api = config.PANDA_API % page
		response = req_sy.get_html(panda_api, is_json=True)
		if response and len(response['data']) != 0:
			res_list = response['data']['items']
			# 是否单个页面爬取在线直播地址
			if is_get_livedata:
				livedatas = _get_page_views_asynchronous(res_list)
			for i, zb in enumerate(res_list):
				kw = {}
				gameHostName = zb['classification']['ename']
				if gameHostName not in game_type:
					continue
				views = int(zb['person_num'])
				if top_views < views:
					top_views = views
				#logger.info(str(page) + ', views:'+ str(views) + ',top_views:'+ str(top_views) + ',' + gameHostName)
				kw['zbid'] = zb['id']
				if is_get_livedata:
					kw['livedata'] = livedatas[zb['id']]
				kw['zbname'] = zb['userinfo']['nickName']
				kw['source'] = onlive_src
				kw['views'] = views
				kw['title'] = zb['name']
				kw['category'] = game_type[gameHostName]
				kw['inputtime'] = int(time.time())
				kw['zb_thumb'] = zb['userinfo']['avatar']
				kw['thumb'] = zb['pictures']['img']
				res_data.append(kw)
		else:
			logger.error("%s | %s" % ("get Panda API Failed", panda_api))
			email.send_email(u'panda直播API获取失败', "%s | %s" % ("get Panda API Failed", panda_api))
			top_views = 100
		page = page + 1

	return res_data

if __name__ == "__main__":
	now =  time.time()
	dbconn = db_sy.getConnection()
	update_data = get_data_action(is_get_livedata = True)
	db_sy.db_update(dbconn, update_data, onlive_src, is_get_livedata=True)
	dbconn.close()
	print "time cost : " + str(int((time.time() - now))) + " seconds"
