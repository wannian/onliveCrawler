#-*-coding:utf-8-*-
#!/usr/bin/python

from lib import db_sy, email_sy
import time
import os

# linux定时任务更改当前路径
os.chdir('/data/live/')
live_platforms = ['huya', 'longzhu', 'douyu', 'huomao', 'zhanqi', 'quanmin']

if __name__ == "__main__":
	now =  time.time()
	dbconn = db_sy.getConnection()
	for platform in live_platforms:
		try:
			module = __import__(platform + '_asynchronous')
			is_get_livedata = True
			print platform + " process begin"
			if platform == 'douyu':
				# 斗鱼每2小时更新图像
				is_get_livedata = False
				local = time.localtime(now)
				# 取消更新斗鱼头像
				if local.tm_min >= 70 and local.tm_hour % 2 == 0:
					is_get_livedata = True
			# 执行方法
			update_data = module.get_data_action(is_get_livedata)
			db_sy.db_update(dbconn, update_data, platform, is_get_livedata)
		except Exception as e:
			reason = "%s : %s" % (u'爬取过程报错，平台'+platform, e)
			email_sy.send_email(u'平台报错', reason)
	dbconn.close()
	print "time cost : " + str(int((time.time() - now))) + " seconds"
	# 发送邮件
	email_sy.send_emails()
	print "Send emails success!"
