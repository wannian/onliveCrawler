/*
Navicat MySQL Data Transfer

Source Server         : 雷神
Source Server Version : 50518
Source Host           : thunderbase.mysql.rds.aliyuncs.com:3306
Source Database       : shenyou

Target Server Type    : MYSQL
Target Server Version : 50518
File Encoding         : 65001

Date: 2016-01-04 09:44:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sy_zhibo
-- ----------------------------
DROP TABLE IF EXISTS `sy_zhibo`;
CREATE TABLE `sy_zhibo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned DEFAULT '0',
  `typeid` smallint(5) unsigned DEFAULT NULL,
  `title` char(150) DEFAULT '' COMMENT '标题',
  `style` char(24) DEFAULT '',
  `thumb` char(200) DEFAULT '' COMMENT '视频截图',
  `keywords` char(40) DEFAULT '' COMMENT '关键字',
  `description` mediumtext COMMENT '描述（保留）',
  `posids` tinyint(1) unsigned DEFAULT '0',
  `url` char(100) DEFAULT NULL COMMENT '保留',
  `listorder` tinyint(3) unsigned DEFAULT '0',
  `status` tinyint(2) unsigned DEFAULT '1',
  `sysadd` tinyint(1) unsigned DEFAULT '0',
  `islink` tinyint(1) unsigned DEFAULT '0' COMMENT '数据保留字段',
  `username` char(20) DEFAULT 'username' COMMENT '名字系统默认',
  `inputtime` int(10) unsigned DEFAULT '0' COMMENT '添加时间',
  `updatetime` int(10) unsigned DEFAULT '0' COMMENT '更新时间',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '主播图像',
  `zbid` varchar(255) DEFAULT '' COMMENT '主播id',
  `zbname` varchar(255) DEFAULT '' COMMENT '主播名字',
  `source` varchar(255) DEFAULT '' COMMENT '来源',
  `hits` varchar(255) DEFAULT '' COMMENT '点击数',
  `category` varchar(255) DEFAULT '' COMMENT '直播类型',
  `isOnlive` varchar(255) DEFAULT '' COMMENT '是否直播',
  `livedata` text COMMENT '直播数据',
  `views` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `status` (`status`,`listorder`,`id`),
  KEY `listorder` (`catid`,`status`,`listorder`,`id`),
  KEY `catid` (`catid`,`status`,`id`),
  KEY `src_zbid` (`source`,`zbid`)
) ENGINE=InnoDB AUTO_INCREMENT=24859 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sy_zhibo
-- ----------------------------
INSERT INTO `sy_zhibo` VALUES ('2463', '9', '8', 'White55开 我女粉丝非常多', '', 'http://staticlive.douyutv.com/upload/web_pic/6/138286_1601032358_thumb.jpg', '', null, '0', null, '0', '99', '0', '0', 'username', '1450096484', '1451836857', 'http://uc.douyutv.com/avatar.php?uid=4757314&size=middle', 'wt55kai', 'White55开解说', 'douyu', '', 'lol', '0', 'http://staticlive.douyutv.com/common/share/play.swf?room_id=138286', '2700000');
INSERT INTO `sy_zhibo` VALUES ('2464', '9', '8', 'yyf直播间  国服5黑', '', 'http://staticlive.douyutv.com/upload/web_pic/8/58428_1601022308_thumb.jpg', '', null, '0', null, '0', '99', '0', '0', 'username', '1450096484', '1451748067', 'http://uc.douyutv.com/avatar.php?uid=236231&size=middle', '58428', 'yyfyyf', 'douyu', '', 'dota2', '0', 'http://staticlive.douyutv.com/common/share/play.swf?room_id=58428', '583000');
INSERT INTO `sy_zhibo` VALUES ('2465', '9', '8', '黑白锐雯,大家新年快乐=-=！', '', 'http://staticlive.douyutv.com/upload/web_pic/2/93912_1601022358_thumb.jpg', '', null, '0', null, '0', '99', '0', '0', 'username', '1450096484', '1451750459', 'http://uc.douyutv.com/avatar.php?uid=3807481&size=middle', '93912', '黑白锐雯', 'douyu', '', 'lol', '0', 'http://staticlive.douyutv.com/common/share/play.swf?room_id=93912', '303000');
INSERT INTO `sy_zhibo` VALUES ('2466', '9', '8', '酥酥：好久没见，想我了吗。', '', 'http://staticlive.douyutv.com/upload/web_pic/5/266055_1601032335_thumb.jpg', '', null, '0', null, '0', '99', '0', '0', 'username', '1450096484', '1451835660', 'http://uc.douyutv.com/avatar.php?uid=1757350&size=middle', '266055', '萌面酥', 'douyu', '', 'lol', '0', 'http://staticlive.douyutv.com/common/share/play.swf?room_id=266055', '108000');
INSERT INTO `sy_zhibo` VALUES ('2467', '9', '8', '板：哇！金色传说！', '', 'http://staticlive.douyutv.com/upload/web_pic/2/101342_1601040038_thumb.jpg', '', null, '0', null, '0', '99', '0', '0', 'username', '1450096484', '1451839255', 'http://uc.douyutv.com/avatar.php?uid=996206&size=middle', 'board', '萌太奇5238', 'douyu', '', 'hearthstone', '0', 'http://staticlive.douyutv.com/common/share/play.swf?room_id=101342', '120000');
INSERT INTO `sy_zhibo` VALUES ('2468', '9', '8', '冬瓜德莱文：发起脾气就砍自己', '', 'http://staticlive.douyutv.com/upload/web_pic/5/221895_1601030016_thumb.jpg', '', null, '0', null, '0', '99', '0', '0', 'username', '1450096484', '1451751651', 'http://uc.douyutv.com/avatar.php?uid=2127453&size=middle', 'JaneEyre', '会旋转的冬瓜', 'douyu', '', 'lol', '0', 'http://staticlive.douyutv.com/common/share/play.swf?room_id=221895', '201000');
INSERT INTO `sy_zhibo` VALUES ('2469', '9', '8', '2016春季赛LPL抽签仪式26日晚上19点', '', 'http://staticlive.douyutv.com/upload/web_pic/6/288016_1512261956_thumb.jpg', '', null, '0', null, '0', '99', '0', '0', 'username', '1450143275', '1451131260', 'http://uc.douyutv.com/avatar.php?uid=19344409&size=middle', 'riot', 'Riot拳头', 'douyu', '', 'lol', '0', 'http://staticlive.douyutv.com/common/share/play.swf?room_id=288016', '51000');
INSERT INTO `sy_zhibo` VALUES ('2470', '9', '8', '爆笑德前来试试效果', '', 'http://staticlive.douyutv.com/upload/web_pic/5/25515_1601040016_thumb.jpg', '', null, '0', null, '0', '99', '0', '0', 'username', '1450096484', '1451838054', 'http://uc.douyutv.com/avatar.php?uid=519190&size=middle', 'qiuri', '秋日丶', 'douyu', '', 'hearthstone', '0', 'http://staticlive.douyutv.com/common/share/play.swf?room_id=25515', '469000');
INSERT INTO `sy_zhibo` VALUES ('2471', '9', '8', '国服盲僧我认第二,  谁敢认第一 ?', '', 'http://staticlive.douyutv.com/upload/web_pic/5/217785_1601032249_thumb.jpg', '', null, '0', null, '0', '99', '0', '0', 'username', '1450096484', '1451833265', 'http://uc.douyutv.com/avatar.php?uid=1206872&size=middle', '217785', 'Night丶杀神风', 'douyu', '', 'lol', '0', 'http://staticlive.douyutv.com/common/share/play.swf?room_id=217785', '185000');
INSERT INTO `sy_zhibo` VALUES ('2472', '9', '8', '天堂火影劫！！！元旦劫快乐', '', 'http://staticlive.douyutv.com/upload/web_pic/2/52_1601040718_thumb.jpg', '', null, '0', null, '0', '99', '0', '0', 'username', '1450096484', '1451863223', 'http://uc.douyutv.com/avatar.php?uid=2192&size=middle', '52', 'LoveAcFun包子', 'douyu', '', 'lol', '0', 'http://staticlive.douyutv.com/common/share/play.swf?room_id=52', '126000');
INSERT INTO `sy_zhibo` VALUES ('2473', '9', '8', 'SoloFeng 大师排位', '', 'http://staticlive.douyutv.com/upload/web_pic/1/319721_1601040032_thumb.jpg', '', null, '0', null, '0', '99', '0', '0', 'username', '1450096484', '1451839255', 'http://uc.douyutv.com/avatar.php?uid=22679518&size=middle', 'lolff', 'solofeng解说丶', 'douyu', '', 'lol', '0', 'http://staticlive.douyutv.com/common/share/play.swf?room_id=319721', '38000');
INSERT INTO `sy_zhibo` VALUES ('2474', '9', '8', '你猜mini有多大？', '', 'http://staticlive.douyutv.com/upload/web_pic/3/10903_1601022331_thumb.jpg', '', null, '0', null, '0', '99', '0', '0', 'username', '1450096484', '1451749263', 'http://uc.douyutv.com/avatar.php?uid=283431&size=middle', '10903', '七煌主持Mini', 'douyu', '', 'lol', '0', 'http://staticlive.douyutv.com/common/share/play.swf?room_id=10903', '185000');
INSERT INTO `sy_zhibo` VALUES ('2475', '9', '8', '八路奶粉：蛮王新套路，一个字，脏.', '', 'http://staticlive.douyutv.com/upload/web_pic/4/19574_1601040935_thumb.jpg', '', null, '0', null, '0', '99', '0', '0', 'username', '1450096484', '1451871630', 'http://uc.douyutv.com/avatar.php?uid=479563&size=middle', '19574', 'lol八路奶粉阿', 'douyu', '', 'lol', '1', 'http://staticlive.douyutv.com/common/share/play.swf?room_id=19574', '106000');
INSERT INTO `sy_zhibo` VALUES ('2476', '9', '8', '飞儿  看看月初的天梯', '', 'http://staticlive.douyutv.com/upload/web_pic/8/265438_1601040010_thumb.jpg', '', null, '0', null, '0', '99', '0', '0', 'username', '1450096484', '1451838054', 'http://uc.douyutv.com/avatar.php?uid=1974429&size=middle', 'liufeier', '刘飞儿faye', 'douyu', '', 'hearthstone', '0', 'http://staticlive.douyutv.com/common/share/play.swf?room_id=265438', '98000');
INSERT INTO `sy_zhibo` VALUES ('2477', '9', '8', '15岁の大莉cc：最强小萝莉(≥◇≤)', '', 'http://staticlive.douyutv.com/upload/web_pic/6/244036_1601032213_thumb.jpg', '', null, '0', null, '0', '99', '0', '0', 'username', '1450096484', '1451830871', 'http://uc.douyutv.com/avatar.php?uid=15226855&size=middle', 'dalicc', '大莉cc', 'douyu', '', 'lol', '0', 'http://staticlive.douyutv.com/common/share/play.swf?room_id=244036', '26000');
INSERT INTO `sy_zhibo` VALUES ('2478', '9', '8', '仙儿：最初が素人', '', 'http://staticlive.douyutv.com/upload/web_pic/3/241823_1601032237_thumb.jpg', '', null, '0', null, '0', '99', '0', '0', 'username', '1450096484', '1451832070', 'http://uc.douyutv.com/avatar.php?uid=13993658&size=middle', 'xianer', '七煌仙儿', 'douyu', '', 'lol', '0', 'http://staticlive.douyutv.com/common/share/play.swf?room_id=241823', '92000');
INSERT INTO `sy_zhibo` VALUES ('2479', '9', '8', '信仰土豪战2级开冲传说', '', 'http://staticlive.douyutv.com/upload/web_pic/4/208114_1601032219_thumb.jpg', '', null, '0', null, '0', '99', '0', '0', 'username', '1450096484', '1451830871', 'http://uc.douyutv.com/avatar.php?uid=9468763&size=middle', 'gouzei', '恩基爱萌萌哒的狗贼', 'douyu', '', 'hearthstone', '0', 'http://staticlive.douyutv.com/common/share/play.swf?room_id=208114', '109000');
INSERT INTO `sy_zhibo` VALUES ('2480', '9', '8', 'Lin小北：水友赛，随意玩下', '', 'http://staticlive.douyutv.com/upload/web_pic/1/436781_1512262337_thumb.jpg', '', null, '0', null, '0', '99', '0', '0', 'username', '1450096484', '1451144458', 'http://uc.douyutv.com/avatar.php?uid=25932670&size=middle', 'linxb', 'Lin小北解说', 'douyu', '', 'lol', '0', 'http://staticlive.douyutv.com/common/share/play.swf?room_id=436781', '114000');
INSERT INTO `sy_zhibo` VALUES ('2481', '9', '8', '蓝猫:豹女 瞎子 男枪 凯瑞型打野专场！', '', 'http://staticlive.douyutv.com/upload/web_pic/7/269517_1512302353_thumb.jpg', '', null, '0', null, '0', '99', '0', '0', 'username', '1450096484', '1451491262', 'http://uc.douyutv.com/avatar.php?uid=14221627&size=middle', '269517', '超威Bluecat丶', 'douyu', '', 'lol', '0', 'http://staticlive.douyutv.com/common/share/play.swf?room_id=269517', '56000');
INSERT INTO `sy_zhibo` VALUES ('2482', '9', '8', '黄金还是白金。。。！', '', 'http://staticlive.douyutv.com/upload/web_pic/4/6324_1601040936_thumb.jpg', '', null, '0', null, '0', '99', '0', '0', 'username', '1450143275', '1451871630', 'http://uc.douyutv.com/avatar.php?uid=212654&size=middle', 'chouxiangTV', '抽象工作室upupup', 'douyu', '', 'lol', '1', 'http://staticlive.douyutv.com/common/share/play.swf?room_id=6324', '42000');
INSERT INTO `sy_zhibo` VALUES ('2483', '9', '8', 'Jackey德莱文.80胜率上王者.!!', '', 'http://staticlive.douyutv.com/upload/web_pic/9/315449_1601032310_thumb.jpg', '', null, '0', null, '0', '99', '0', '0', 'username', '1450096484', '1451834422', 'http://uc.douyutv.com/avatar.php?uid=22064571&size=middle', '315449', 'JackeyLove', 'douyu', '', 'lol', '0', 'http://staticlive.douyutv.com/common/share/play.swf?room_id=315449', '155000');
INSERT INTO `sy_zhibo` VALUES ('2484', '9', '8', '你莉归来之时~', '', 'http://staticlive.douyutv.com/upload/web_pic/2/200752_1601022350_thumb.jpg', '', null, '0', null, '0', '99', '0', '0', 'username', '1450096484', '1451750459', 'http://uc.douyutv.com/avatar.php?uid=5509207&size=middle', '200752', '莉爷酱丶', 'douyu', '', 'lol', '0', 'http://staticlive.douyutv.com/common/share/play.swf?room_id=200752', '175000');
INSERT INTO `sy_zhibo` VALUES ('2485', '9', '8', 'ZSMJ直播 冲分', '', 'http://staticlive.douyutv.com/upload/web_pic/6/52876_1601040934_thumb.jpg', '', null, '0', null, '0', '99', '0', '0', 'username', '1450096484', '1451871630', 'http://uc.douyutv.com/avatar.php?uid=1155301&size=middle', 'ZSMJ', 'ZSMJ727974758', 'douyu', '', 'dota2', '1', 'http://staticlive.douyutv.com/common/share/play.swf?room_id=52876', '25000');
