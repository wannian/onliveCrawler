#-*-coding:utf-8-*-
#!/usr/bin/python

from lib import config, req_sy, db_sy
from lib import email_sy as email
from lib.logger import logger
from bs4 import BeautifulSoup
import time
import re
import gevent.monkey
gevent.monkey.patch_socket()
import gevent
from gevent.pool import Pool
from types import *

onlive_src = 'huomao'
gpool = Pool(config.GPOOLSIZE)

# 处理来源于火猫TV的直播视频，直播地址： http://www.huomaotv.com/live/907
def _pro_onlivedata_from_huomao(zbid):
	url = config.HUOMAO_VIEW % zbid
	#logger.info('%s | %s ' % (u'huomao', url))
	html = req_sy.get_html(url)
	if html.strip() != '':
		try:
			soup = BeautifulSoup(html, 'html.parser', from_encoding = 'utf-8')
			# 查找当前直播地址
			text = soup.find(text = re.compile("video_name"))
			matchObj = re.search(r'var\s+video_name.*\'(.*)\';', text)
			if matchObj:
				VideoIDS = matchObj.group(1)
				# src="http://swf.huomaotv.com/huomaoplayerv2.swf?ver=20150721&amp;initSize=big&amp;VideoIDS=Ct_FT9PPzbDNbR1cN10&amp;live_id=5325&amp;streamtype=live&amp;autostart=true&amp;url=http://www.huomaotv.com&amp;s=swf&amp;g=get_video_data&amp;hasDanmu=0&amp;hasFitPage=0&amp;hasLowBitrate=1&amp;hasFeedBack=0&amp;hasRefresh=1"
				return zbid, 'http://swf.huomaotv.com/huomaoplayerv2.swf?ver=20150721&amp;initSize=big&amp;VideoIDS=' + str(VideoIDS) + '&amp;live_id=' + str(zbid) + '&amp;streamtype=live&amp;autostart=true&amp;url=http://www.huomaotv.com&amp;s=swf&amp;g=get_video_data&amp;hasDanmu=0&amp;hasFitPage=0&amp;hasLowBitrate=1&amp;hasFeedBack=0&amp;hasRefresh=1'
		except Exception as e:
			reason = "%s : %s, zbid:%s" % (u'获取huomao直播地址', e, zbid)
			logger.error(reason)
			email.send_email(u'huomao直播爬取error', reason)
	return zbid, ''

def _get_page_views_asynchronous(items):
	threads = []
	if type(items) is ListType:
		for zb in items:
			zbid = zb['id']
			threads.append(gpool.spawn(_pro_onlivedata_from_huomao, zbid))
	elif type(items) is DictType:
		for key in items:
			zbid = items[key]['id']
			threads.append(gpool.spawn(_pro_onlivedata_from_huomao, zbid))
	gpool.join()

	res = {}
	is_error = False
	reason = None
	# 遍历threads, 判断是否成功
	for thread in threads:
		if thread.successful():
			res[thread.value[0]] = thread.value[1]
		else:
			is_error = True
			reason = thread.exception
	# 若执行过程出错，发送邮件
	# if is_error:
	# 	email.send_email(u'huomao直播爬取error', str(reason))
	return res

def get_data_action(is_get_livedata = True):
	game_type = config.HUOMAO_GAME
	# 火猫TV视频列表页api: http://www.huomaotv.com/channel/all?ajax=1&p=1
	page = 1
	# 在线人数作为是否继续爬取的判断条件
	top_views = 100
	res_data = []
	while page < 200 and top_views > 30:
		time.sleep(0.3)
		top_views = 29
		huomao_api = config.HUOMAO_API % page
		response = req_sy.get_html(huomao_api, is_json=True)
		print huomao_api
		if response and len(response) != 0:
			res_list = response
			# 是否单个页面爬取在线直播地址
			if is_get_livedata:
				livedatas = _get_page_views_asynchronous(res_list)
			if type(res_list) is ListType:
				for i, zb in enumerate(res_list):
					kw = {}
					# 判断是否在直播
					is_live = zb['is_live']
					if is_live == '0':
						continue
					gameHostName = zb['gid']
					if gameHostName not in game_type:
						continue
					views = int(zb['views'])
					if top_views < views:
						top_views = views
					#logger.info(str(page) + ', views:'+ str(views) + ',top_views:'+ str(top_views) + ',' + gameHostName)
					kw['zbid'] = zb['id']
					if is_get_livedata:
						kw['livedata'] = livedatas[zb['id']]
					kw['zbname'] = zb['username']
					kw['source'] = onlive_src
					kw['views'] = views
					kw['title'] = zb['channel']
					kw['category'] = game_type[gameHostName]
					kw['inputtime'] = int(time.time())
					# 更改用户头像尺寸
					kw['zb_thumb'] = zb['head_img'].replace('small','middle')
					kw['thumb'] = zb['img']
					res_data.append(kw)
			else:
				for key in res_list:
					zb = res_list[key]
					kw = {}
					# 判断是否在直播
					is_live = zb['is_live']
					if is_live == '0':
						continue
					gameHostName = zb['gid']
					if gameHostName not in game_type:
						continue
					views = int(zb['views'])
					if top_views < views:
						top_views = views
					#logger.info(str(page) + ', views:'+ str(views) + ',top_views:'+ str(top_views) + ',' + gameHostName)
					kw['zbid'] = zb['id']
					if is_get_livedata:
						kw['livedata'] = livedatas[zb['id']]
					kw['zbname'] = zb['username']
					kw['source'] = onlive_src
					kw['views'] = views
					kw['title'] = zb['channel']
					kw['category'] = game_type[gameHostName]
					kw['inputtime'] = int(time.time())
					# 更改用户头像尺寸
					kw['zb_thumb'] = zb['head_img'].replace('small','middle')
					kw['thumb'] = zb['img']
					res_data.append(kw)
		else:
			logger.error("%s | %s" % ("get "+onlive_src+" API Failed", huomao_api))
			email.send_email(onlive_src+u'直播API获取失败', "%s | %s" % ("get "+onlive_src+" API Failed", huomao_api))
			top_views = 100
		page = page + 1

	return res_data

if __name__ == "__main__":
	now =  time.time()
	dbconn = db_sy.getConnection()
	update_data = get_data_action(is_get_livedata = True)
	db_sy.db_insert(dbconn, update_data)
	dbconn.close()
	print "time cost : " + str(int((time.time() - now))) + " seconds"
