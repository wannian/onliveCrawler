#-*-coding:utf-8-*-
#!/usr/bin/python

from lib import config, req_sy
from bs4 import BeautifulSoup
import codecs
import onliveDown
import time
import re

onlive_src = 'panda'

# 处理来源熊猫的的直播视频 直播地址：http://www.panda.tv/666666
def pro_onlivedata_from_panda(zbid, room_key, f_url_ex, url_pre = 'http://www.panda.tv/'):
	url = url_pre + zbid
	print u'panda, ' + url
	html = req_sy.get_html(url)
	time.sleep(0.2)
	if html.strip() != '':
		soup = BeautifulSoup(html, 'html.parser', from_encoding = 'utf-8')
		# 查找当前直播地址
		text = soup.find(text = re.compile("PANDA_PLAYER_URL"))
		matchObj = re.search(r'URL\":\"(.*)\",', text)
		if matchObj:
			player_url = matchObj.group(1)
			# src = "http://s4.pdim.gs/static/1a788bf39f2f5050.swf?sign=true&amp;videoId=92d768563027d708b0cb81793d93b3bf&amp;
			return player_url + "?sign=true&amp;videoId=" + room_key + "&amp;isAnchor=false&amp;plflag=2_3"
		else:
			return ''

def panda_insert_action(dbconn):
	game_type = config.PANDA_GAME
	file_name = onlive_src + '.log'
	file_err_name = onlive_src + '_err.log'
	file_url_ex = onlive_src + '_pagedown_err.log'
	f = codecs.open(file_name, 'w', 'utf8')
	f_err = codecs.open(file_err_name, 'a+', 'utf8')
	f_url_ex = codecs.open(file_url_ex, 'a+', 'utf8')
	page = 1
	# panda视频列表页api:http://www.panda.tv/live_lists?status=2&order=person_num&pageno=2
	# 在线人数作为是否继续爬取的判断条件
	top_views = 100
	while page < 200 and top_views > 50:
		top_views = 49
		cursor = dbconn.cursor()
		huya_api = config.PANDA_API % page
		response = req_sy.get_html(huya_api, is_json=True)
		if response and len(response['data']) != 0:
			res_list = response['data']['items']
			for zb in res_list:
				gameHostName = zb['classification']['ename']
				if gameHostName not in game_type:
					continue
				views = int(zb['person_num'])
				if top_views < views:
					top_views = views
				print str(page) + ', views:'+ str(views) + ',top_views:'+ str(top_views) + ',' + gameHostName +'\r\n'
				zbid = zb['id']
				room_key = zb['room_key']
				livedata = pro_onlivedata_from_panda(zbid, room_key, f_url_ex)
				zbname = zb['userinfo']['nickName']
				source = onlive_src
				title = zb['name']
				category = game_type[gameHostName]
				inputtime = int(time.time())
				zb_thumb = zb['userinfo']['avatar']
				thumb = zb['pictures']['img']
				try:
					cursor.execute("INSERT INTO `sy_video_onlive` (`zbid`, `zbname`, `source`, `title`, `views`, `category`, `isOnlive`, `inputtime`, `zb_thumb`, `thumb`, `livedata`) VALUES (%s, %s, %s, %s, %s, %s, 1, %s, %s, %s, %s)",(zbid, zbname, source, title, views, category, inputtime, zb_thumb, thumb, livedata))
				except Exception, e2:
					print Exception, e2
					print >> f_err, Exception, e2
				dbconn.commit()
		cursor.close()
		page = page + 1
	f.close()
	f_err.close()

if __name__ == "__main__":
	now =  time.time()
	dbconn = onliveDown.getConnection()
	panda_insert_action(dbconn)
	print "time cost : " + str(int((time.time() - now))) + " seconds"
